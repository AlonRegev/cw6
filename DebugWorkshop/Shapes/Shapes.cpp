#include "shape.h"
#include "triangle.h"
#include <iostream>


int main() 
{
	Triangle triangle(2, 10);
	std::cout << "The area of the triangle is " << triangle.get_area() << std::endl;

	Shape* pTriangle = new Triangle(2, 10);
	std::cout << "The area of the triangle is " << ((Triangle*)pTriangle)->get_area() << std::endl;	// function can't be virtual, using casting to refer to triangle's method 
	delete pTriangle;

	return 0;
}
